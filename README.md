# libcards

A library for making playing card games!

## Deck
the "Deck" class is the deck of cards.
### Types
name | type | description

`gen` `function` Generates the deck. creates 4 sets of every card.

`shuffle` `function` Shuffles the deck.

`cards` `array` Returns every card in the deck.

## Hand

### Types

name | type | description

`draw` `function` Draws a card from the specified `Deck` object.

`cards` `Array` Shows the cards in the hand.
